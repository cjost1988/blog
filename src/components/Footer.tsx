import * as React from "react";
import Dialog from "@material-ui/core/Dialog/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import Button from "@material-ui/core/Button/Button";

interface FooterPropsInterface {

}

interface FooterStateInterface {
    showDialog: boolean;
}

class Footer extends React.Component<FooterPropsInterface, FooterStateInterface>
{

    constructor(props: FooterPropsInterface) {
        super(props);
        this.state = {showDialog: false};
    }

    render(): JSX.Element
    {
        return (
            <div className="footer">
                <Dialog
                    fullScreen={false}
                    open={this.state.showDialog}
                    onClose={this.handleCloseDialog}
                    aria-labelledby="responsive-dialog-title"
                >
                    <DialogTitle id="responsive-dialog-title">
                        Contact
                    </DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Christian Jost
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleCloseDialog} color="primary" autoFocus>
                            Close
                        </Button>
                    </DialogActions>
                </Dialog>
                <a href="#" onClick={(event: any) => {
                    event.preventDefault();
                    this.setState({showDialog: true});
                }}>Contact</a>
            </div>
        );
    }

    handleCloseDialog = (event: any) => {
        this.setState({showDialog: false});
    };
}

export default Footer;
