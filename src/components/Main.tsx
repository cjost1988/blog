import * as React from "react";
import Paper from "@material-ui/core/Paper/Paper";
import withStyles, {WithStyles} from "@material-ui/core/styles/withStyles";
import Avatar from "@material-ui/core/Avatar/Avatar";
import Grid from "@material-ui/core/Grid/Grid";
import Chip from "@material-ui/core/Chip/Chip";
import Icon from "@material-ui/core/Icon/Icon";
import Button from "@material-ui/core/Button/Button";

const styles = (theme: any) => ({
    paper: theme.mixins.gutters({
        paddingTop: 16,
        paddingBottom: 16,
    }),
    icon: {
        marginLeft: '4px'
    },
    button: {
        marginRight: '4px',
        marginBottom: '4px',
    }
});

interface Contact {
    label: string;
    icon: string;
    value: string;
    newWindow: boolean;
}

class Main extends React.Component<WithStyles<"paper"|"icon"|"button">>
{
    private contacts: Array<Contact> = [
        {
            label: 'Email',
            icon: 'email',
            value: 'mailto: me@cjost.de',
            newWindow: false,
        },
        {
            label: 'Xing',
            icon: 'home',
            value: 'https://www.xing.com/profile/Christian_Jost16',
            newWindow: true,
        },
        {
            label: 'GitHub',
            icon: 'code',
            value: 'https://github.com/cjost1988',
            newWindow: true,
        }
    ];

    render(): JSX.Element
    {
        const
            {classes} = this.props,
            contacts = this
                .contacts
                .map((contact: Contact, index: number) => (
                    <Button
                        key={index}
                        variant="outlined"
                        className={classes.button}
                        onClick={() => this.handleContactClick(contact)}>
                        {contact.label}
                        <Icon className={classes.icon}>{contact.icon}</Icon>
                    </Button>
                )
            )
        ;

        return <Paper className={classes.paper}>
            <Grid container>
                <Grid item xs={3}>
                    <Avatar style={{width: '80px', height: '80px'}} alt="Remy Sharp" src="images/me.jpg" />
                </Grid>
                <Grid item xs={9}>
                    <div><h1 className="page-title">Christian Jost</h1></div>
                    <ul style={{paddingLeft: '20px'}}>
                        <li>Software developer, Instructor for IT apprenticeships</li>
                        <li>
                            Currently working for&nbsp;
                            <span>
                                <a href="https://mapudo.com" rel="noreferrer" title="Mapudo GmbH">mapudo.com</a>
                            </span>
                        </li>
                    </ul>
                    <div>
                        {contacts}
                    </div>
                </Grid>
            </Grid>
        </Paper>;
    }

    handleContactClick = (contact: Contact) => {
        contact.newWindow ?
            window.open(contact.value) :
            window.location.href = contact.value;
    };
}

export default withStyles(styles)(Main);
