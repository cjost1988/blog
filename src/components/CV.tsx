import ExpansionPanel from "@material-ui/core/ExpansionPanel/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary/ExpansionPanelSummary";
import Typography from "@material-ui/core/Typography/Typography";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails/ExpansionPanelDetails";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import * as React from "react";
import withStyles, {WithStyles} from "@material-ui/core/styles/withStyles";
import Chip from "@material-ui/core/Chip/Chip";

const styles = (theme: any) => ({
    root: {
        width: '100%',
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        fontWeight: theme.typography.fontWeightRegular,
    },
    list: {
        padding: '0 20px'
    }
});

interface Technology {
    label: string;
}

class CV extends React.Component<WithStyles<"root"|"heading"|"list">>
{
    private technologies: Array<Technology> = [
        {label: 'PHP'},
        {label: 'Javascript'},
        {label: 'HTML'},
        {label: 'CSS'},
        {label: 'SASS/SCSS'},
        {label: 'MySQL'},
        {label: 'PostgreSQL'},
        {label: 'MongoDB'},
        {label: 'RabbitMQ'},
        {label: 'Elasticsearch'},
        {label: 'ReactJS'},
        {label: 'AngularJS'},
        {label: 'Git'},
        {label: 'Symfony'},
        {label: 'Zend Framework'},
        {label: 'Webpack'},
        {label: 'Gulp'},
        {label: 'Docker'},
        {label: 'Kubernetes'},
        {label: 'Rancher'},
        {label: 'AWS'},
    ];

    render(): JSX.Element
    {
        const
            {classes} = this.props,
            technologies = this
                .technologies
                .map((tech: Technology, index: number) => <Chip key={index} label={tech.label} className="technology" />)
        ;

        return (
            <div className="cv">
                <ExpansionPanel>
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                        <Typography className={classes.heading}>Technologies</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <Typography>
                            {technologies}
                        </Typography>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel>
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                        <Typography className={classes.heading}>Employers</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <Typography>
                            ...
                        </Typography>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel>
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                        <Typography className={classes.heading}>Tech-Stack of this page</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails style={{flexDirection: 'column'}}>
                        <div><b>Code:</b></div>
                        <ul className={classes.list}>
                            <li>Static page</li>
                            <li>Javascript / Typescript / SASS / CSS</li>
                            <li>ReactJS</li>
                            <li>Webpack to compile assets</li>
                            {/*<li>Code can be found at <a href="#" title="GitLab">GitLab</a></li>*/}
                        </ul>
                        <div><b>Infrastructure:</b></div>
                        <ul className={classes.list}>
                            <li>Compiled into a stateless Docker image</li>
                            <li>Running as Docker container in a Kubernetes cluster</li>
                            <li>Using Kubernetes Ingress for SSL termination</li>
                            <li>SSL certificate obtained from letsencrypt with auto renewal functionality</li>
                        </ul>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
            </div>
        );
    }
}

export default withStyles(styles)(CV);
