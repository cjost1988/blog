import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Main from "./components/Main";
import Grid from "@material-ui/core/Grid/Grid";
import './styles/main.scss';
import CV from "./components/CV";
import Footer from "./components/Footer";

ReactDOM.render(
    <Grid container spacing={16}>
        <Grid item xs={12}>
            <Main />
        </Grid>
        <Grid item xs={12}>
            <CV />
        </Grid>
        <Grid item xs={12} style={{textAlign: 'right'}}>
            <Footer />
        </Grid>
    </Grid>,
    document.getElementById('main')
);
